# Ascension Teaching Factory Website

## Description

The Teaching Factory Attendance Website is a digital platform designed to facilitate the process of recording the attendance of members of the Teaching Factory organization. This website provides various features that make attendance management and real-time reporting easier.

### Installation

1. Clone the repository

```sh
git clone https://gitlab.com/ryansyukur.it/tefa.git
```

2. Navigate to the repository directory

```sh
cd tefa
```

3. Install the dependencies

```sh
composer install
```

4. Generate Key

```sh
php artisan key:generate
```

5. Run app 

```sh
php artisan key:generate
```

## Built With

```

- [Laravel](https://laravel.com/)
- [Bootstrap](https://getbootstrap.com/)

```
